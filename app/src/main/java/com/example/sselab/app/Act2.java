package com.example.sselab.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act2", "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2);
    }

    public void onClickUpper(View v) {
        Log.i("Act2.btn_act2_go1", "onClickUpper");

        Intent intent1 = new Intent(Act2.this, Act1.class);
        startActivity(intent1);
    }

    public void onClickLower(View v) {
        Log.i("Act2.btn_act2_go3", "onClickLower");

        Intent intent1 = new Intent(Act2.this, Act3.class);
        startActivity(intent1);
    }
}
