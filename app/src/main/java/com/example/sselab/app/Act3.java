package com.example.sselab.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act3", "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3);
    }

    public void onClickUpper(View v) {
        Log.i("Act3.btn_act3_go2", "onClickUpper");

        Intent intent1 = new Intent(Act3.this, Act2.class);
        startActivity(intent1);
    }

    public void onClickLower(View v) {
        Log.i("Act3.btn_act3_go5", "onClickLower");

        Intent intent1 = new Intent(Act3.this, Act4.class);
        startActivity(intent1);
    }
}
