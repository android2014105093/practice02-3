package com.example.sselab.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act4", "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act4);
    }

    public void onClick(View v) {
        Log.i("Act4.btn_act4_go3", "onClick");

        Intent intent1 = new Intent(Act4.this, Act3.class);
        startActivity(intent1);
    }
}
