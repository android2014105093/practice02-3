package com.example.sselab.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act1", "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);
    }

    public void onClick(View v) {
        Log.i("Act1.btn_act1_go2", "onClick");

        Intent intent1 = new Intent(Act1.this, Act2.class);
        startActivity(intent1);
    }

    public void onClickPrint(View v) {
        Log.i("Act1.btn_printName", "onClickPrint");

        TextView temp = (TextView) findViewById(R.id.textView5);
        temp.setVisibility(View.VISIBLE);
    }
}
